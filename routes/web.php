<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EquipoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

//estas solo acceden a una ruta especifica
/*Route::get('/equipos', function () {
    return view('equipos.index');
});

Route::get('/equipos/create', [EquipoController::class, 'create']);
*/
//esta ruta accede a todas las rutas

Route::resource('equipos', EquipoController::class);
Auth::routes();


Route::get('/home', [EquipoController::class, 'index'])->name('home');
/*
Route::middleware(['auth'])->group(function () {
    Route::get('/home', [EquipoController::class, 'index'])->name('home');
});*/
Route::group(['middleware'=>'auth'],function () {
    Route::get('/', [EquipoController::class, 'index'])->name('home');
});