<h1> {{ $modo }} Equipo </h1>
<label for="nombre">nombre</label>
<input class="form-control" type="text" name="nombre" id="nombre"
    value="{{ isset($equipo->nombre) ? $equipo->nombre : '' }}">
<br>
<label for="modelo">modelo</label>
<input class="form-control" type="text" name="modelo" id="modelo"
    value="{{ isset($equipo->modelo) ? $equipo->modelo : '' }}">
<br>
<label for="marca">marca</label>
<input class="form-control" type="text" name="marca" id="marca"
    value="{{ isset($equipo->marca) ? $equipo->marca : '' }}">
<br>
<label for="correo">correo</label>
<input class="form-control" type="text" name="correo" id="correo"
    value="{{ isset($equipo->correo) ? $equipo->correo : '' }}">
<br>
@if (isset($equipo->imagen))
    <img src="{{ asset('storage') . '/' . $equipo->imagen }}" width="200" alt="">
@endif
<input class="form-control" type="file" name="imagen" id="imagen">
<br>
<input class="form-control" type="submit" value="{{ $modo }}">
<a href="{{ url('/equipos') }}">Cancelar</a>
