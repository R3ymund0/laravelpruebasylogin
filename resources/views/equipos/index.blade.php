mostrar la lista de empleados del index

@if (Session::has('menssaje'))
    {{ session::get('menssaje') }}
@endif
<a href="{{ url('/equipos/create') }}">Crear Equipo nuevo</a>
<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>imagen</th>
            <th>nombre</th>
            <th>modelo</th>
            <th>marca</th>
            <th>correo</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($equipos as $equipo)
            <tr>
                <td>{{ $equipo->id }}</td>
                <td><img src="{{ asset('storage') . '/' . $equipo->imagen }}" width="200" alt="">
                </td>
                <td>{{ $equipo->nombre }}</td>
                <td>{{ $equipo->modelo }}</td>
                <td>{{ $equipo->marca }}</td>
                <td>{{ $equipo->correo }}</td>
                <td><a href="{{ url('/equipos/' . $equipo->id . '/edit') }}">Editar</a> |

                    <form action="{{ url('/equipos/' . $equipo->id) }}" method="post">
                        @csrf
                        {{ method_field('DELETE') }}
                        <input class="form-control" type="submit" value="Borrar">
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
