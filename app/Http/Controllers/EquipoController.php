<?php

namespace App\Http\Controllers;

use App\Models\equipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EquipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['equipos']=equipo::paginate(5);
        return view('equipos.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('equipos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$equipo = request()->all();
        $equipo = request()->except('_token');
        if($request->hasFile('imagen')){
            $equipo['imagen']=$request->file('imagen')->store('uploads','public');
        }
        equipo::insert($equipo);
        //return response()->json($equipo);
        return redirect('equipos')->with('menssaje', 'Agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function show(equipo $equipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equipo = equipo::find($id);
        return view('equipos.edit', ['equipo' => $equipo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $equipo = request()->except(['_token','_method']);

        if($request->hasFile('imagen')){
            $equipoimagen = equipo::find($id);
            Storage::delete('public/'.$equipoimagen->imagen);
            $equipo['imagen']=$request->file('imagen')->store('uploads','public');
        }

        equipo::where('id','=',$id)->update($equipo);

        $equipo = equipo::find($id);
        return view('equipos.edit', ['equipo' => $equipo]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equipo = equipo::find($id);
        if(Storage::delete('public/'.$equipo->imagen)){
            equipo::destroy($id);
        }
        
        return redirect('equipos')->with('menssaje', 'Eliminado exitosamente');
    }
}
